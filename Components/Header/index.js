import React from "react";
import { Text, View } from "react-native";
import styles from "./style";

export default function index({ onPress }) {
  return (
    <View style={styles.subCard}>
      <Text style={styles.cardTextTitle}> Jad Media Production </Text>
      <Text style={styles.cardTextSubTitle}> Since 2019 </Text>
    </View>
  );
}
