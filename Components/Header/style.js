import { StyleSheet } from "react-native";
export default StyleSheet.create({
      cardTextTitle:{
        color:'white',
        alignSelf:'flex-start',
        marginTop:20,
        fontFamily:'MonteserratBold',
        fontSize:17
      },
      cardTextSubTitle:{
        fontFamily:'MonteserratRegular',
        color:'white'
      },
      subCard:{
        backgroundColor:'#222F3E',
        height:'10%',
        width:'95%',
        borderBottomEndRadius:15,
        borderBottomLeftRadius:15,
        borderTopLeftRadius:15,
        borderTopRightRadius:15
      }
});
