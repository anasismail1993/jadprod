import React from 'react'
import { Text, TouchableOpacity, Image} from 'react-native'
import styles from './style'

export default function index({ onPress }) {
    return (
        <TouchableOpacity style={styles.card} onPress={() => onPress()}>
        <Image style={styles.CardImage} source={require("../../../assets/images/selfie.png")} />
        <Text style={styles.cardTextTitle}> Request your own work</Text>
        <Text style={styles.cardTextSubTitle}> Contact us here </Text>
        </TouchableOpacity>
    );
  }
  