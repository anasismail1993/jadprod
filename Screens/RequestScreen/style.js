import React from 'react'
import { StyleSheet } from 'react-native'

export default StyleSheet.create( {
    
        container: {
          flex: 1,
          backgroundColor: '#161E29',
          alignItems: 'center',
          justifyContent: 'center',
        }
    
})
