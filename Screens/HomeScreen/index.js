import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {  Text, View, Image, TouchableOpacity,FlatList} from 'react-native';
import styles from './style'
import ProjectsCard from "../../Components/Card/ProjectsCard";
import RequestCard from "../../Components/Card/RequestCard";
import Header from "../../Components/Header"

export default function HomeScreen({ navigation }) {
  
  return (
    <View style={styles.container}>
      <Header />
      <View style={{width:'100%',height:60}}></View>
      <ProjectsCard onPress={() => navigation.navigate("Projects")} />
      <View style={{width:'100%',height:60}}></View>
      <RequestCard onPress={() => navigation.navigate("Request")}/>
    
    </View>
  );
}



