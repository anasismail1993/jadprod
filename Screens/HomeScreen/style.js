import React from 'react'
import { StyleSheet } from 'react-native'

export default StyleSheet.create( {
    
        container: {
          flex: 1,
          backgroundColor: '#161E29',
          alignItems: 'center',
          justifyContent: 'center'
        },
        card:{
          backgroundColor:'#7140FC',
          height:'30%',
          width:'95%',
          borderBottomEndRadius:15,
          borderBottomLeftRadius:15,
          borderTopLeftRadius:15,
          borderTopRightRadius:15,
          marginTop:80
        },
        cardTextTitle:{
          color:'white',
          alignSelf:'flex-start',
          marginTop:20,
          fontFamily:'MonteserratBold'
        },
        cardTextSubTitle:{
          fontFamily:'MonteserratRegular'
        },
        subCard:{
          marginTop:20,
          backgroundColor:'#222F3E',
          height:'15%',
          width:'40%',
          borderBottomEndRadius:15,
          borderBottomLeftRadius:15,
          borderTopLeftRadius:15,
          borderTopRightRadius:15
        },
        CardImage:{
          width:'60%',
          height:'90%',
          position:'absolute',
          alignSelf:'flex-end',
          marginTop:80
        }
    
})
