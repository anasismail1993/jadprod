import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { AppStack } from "./navigation";
import { StyleSheet, Text, View } from 'react-native';
import  AppLoading  from "expo-app-loading";
import { useFonts } from "@expo-google-fonts/inter";


const customFonts = {
  MonteserratBold: require("./assets/fonts/Montserrat/Montserrat-Bold.ttf"),
  MonteserratRegular: require("./assets/fonts/Montserrat/Montserrat-Regular.ttf"),
  MonteserratMedium: require("./assets/fonts/Montserrat/Montserrat-Medium.ttf"),
  MonteserratSmall: require("./assets/fonts/Montserrat/Montserrat-Thin.ttf"),
};
const App = () => {

  const [isLoaded] = useFonts(customFonts);
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <AppStack />
    );
  }
    }
  

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
